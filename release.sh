#!/bin/sh

#获取版本号
ver=`cat setup.py | grep "ver = " | awk '{print $3}' | sed "s/'//g"`
echo ${ver}

#编译打包
python setup.py sdist build

filename=tweb-sdk-${ver}.tar.gz

#上传到PyPi
twine upload dist/${filename}
