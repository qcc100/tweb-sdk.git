#!/bin/sh

# default profiles
# environment：dev|prod
ENV="prod"

# logging level: debug|info|warning|error|none
LOGGING="info"

BASEDIR=`pwd`
APP_NAME=$(basename ${BASEDIR})
MAIN_PY=main.py

cd ${BASEDIR}

case "$1" in
    start)
    	procedure=`ps -ef | grep -w "${BASEDIR}" |grep -w "python"| grep -v "grep" | awk '{print $2}'`
		if [ "${procedure}" = "" ];
		then
			echo "${APP_NAME} start ..."
			exec nohup ./venv/bin/python ${BASEDIR}/${MAIN_PY} --logging=${LOGGING} --env=${ENV} --log-file-prefix=log/${APP_NAME}.log &
		else
			echo "${APP_NAME} was start"
		fi
		;;
    run)
        procedure=`ps -ef | grep -w "${BASEDIR}" |grep -w "python"| grep -v "grep" | awk '{print $2}'`
		if [ "${procedure}" = "" ];
		then
			echo "${APP_NAME} start ..."
			exec ./venv/bin/python ${BASEDIR}/${MAIN_PY} --logging=${LOGGING} --env=${ENV} &
		else
			echo "${APP_NAME} was start"
		fi
		;;
    stop)
    	procedure=`ps -ef | grep -w "${BASEDIR}" |grep -w "python"| grep -v "grep" | awk '{print $2}'`
		if [ "${procedure}" = "" ];
		then
			echo "${APP_NAME} was stop"
		else
			kill ${procedure}
			sleep 2
			arg_procedure=`ps -ef | grep -w "${BASEDIR}" |grep -w "python"| grep -v "grep" | awk '{print $2}'`
			if [ "${arg_procedure}" = "" ];
			then
				echo "${APP_NAME}(${procedure}) stop success"
			else
				kill -9 ${arg_procedure}
				echo "${APP_NAME} stop error"
			fi
		fi
		rm -f nohup.out
		;;
    *)
        echo "usage: $0 [start|run|stop]"
		;;
esac
exit 0