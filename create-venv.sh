#!/bin/sh

platform=`which python3 | awk '{print $1}'`

if [ -z "${platform}" ]; then
    echo "Not install python3"
else
    virtualenv -p ${platform} venv
fi

exit 0
