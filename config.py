# coding=utf-8

# 服务实例ID，集群部署时，每个实例需要不同的ID
ID = 1

# 监听地址和端口
Host = '0.0.0.0'
Port = 16000

# API相关
VER = 'v1'
PLATFORM = 'qcc'

# 语言版本，与错误提示信息等有关
LANG = 'zh'  # en/zh

# 应用启动配置
# cookie_secret = str(base64.b85encode(os.urandom(20)), encoding="utf8")
TornadoSettings = {
    'template_path': 'views',
    'static_path': 'statics',
    'cookie_secret': 'sKjie*&#9w_jsdfk&(002',
    'login_url': "/{}/{}/uc/login/pwd".format(VER, PLATFORM),
    'autoreload': False,
    'debug': True
}

# Mysql配置
Mysql = {
    'active': False,    # 是否启用
    'dev': {
        'host': 'localhost',
        'port': 3306,
        'user': 'uc',
        'pwd': 'uc2app',
        'charset': 'utf8'
    },
    'prod': {
        'host': 'localhost',
        'port': 3306,
        'user': 'uc',
        'pwd': 'uc2app',
        'charset': 'utf8'
    }
}

Redis = {
    'active': True,    # 是否启用
    'dev': {
        'host': 'localhost',
        'port': '6379'
    },
    'prod': {
        'host': 'localhost',
        'port': '6379'
    }
}


# Kafka servers
KafkaServers = ['47.52.189.143:9092']


"""
以下是与应用相关的配置
"""
